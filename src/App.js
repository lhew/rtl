import React, { Component } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import store from "./store";
import "./App.css";
import Home from "./components/home/Home";
import Results from "./components/results/Results";
import Show from "./components/show/Show";
import Episode from "./components/episode/Episode";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <h1 className="title-block"><Link to="/">TV Show finder</Link></h1>
            <Route path="/" exact component={Home} />
            <Route path="/results/" component={Results} />
            <Route path="/show/:show" exact component={Show} />
            <Route path={"/show/:show/episode/:season/:chapter"} exact component={Episode} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
