import {GET_SHOWS, GET_SHOW, GET_EPISODES, GET_EPISODE} from '../constants';

export const getShows = showName => ({type: GET_SHOWS, showName});
export const getShow = showID => ({type: GET_SHOW, showID});
export const getEpisodes = showID => ({type: GET_EPISODES, showID});
export const getEpisode = (showID, season, episode) => ({type: GET_EPISODE, showID, season, episode});