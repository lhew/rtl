import axios from "axios";
import { SHOWS_PATH, SINGLE_SHOW_PATH } from "../constants";

export const requestShows = async showName => {
  const request = await axios.get(`${SHOWS_PATH}?q=${showName}`);
  try {
    const response = await request.data;
    return response;
  } catch (e) {
    return new Error("Error: " + e);
  }
};

export const requestShow = async showID => {
  const request = await axios.get(`${SINGLE_SHOW_PATH}${showID}`);
  const response = await request.data;
  return response;
};

export const requestEpisodes = async showID => {
  const request = await axios.get(`${SINGLE_SHOW_PATH}${showID}/episodes`);
  const response = await request.data;
  return response;
};

export const requestEpisode = async (showID, seasonID, episodeID) => {
  const request = await axios.get(
    `${SINGLE_SHOW_PATH}${showID}/episodebynumber?season=${seasonID}&number=${episodeID}`
  );
  const response = await request.data;
  return response;
};
