import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getShow, getEpisode } from "../../actions";
import Home from "../home/Home";
import EpisodeResult from "./EpisodeResult";

class Episode extends React.Component {
  componentDidMount() {
    const {
      show,
      showId,
      seasonId,
      episodeId,
      getShow,
      getEpisode,
      episode
    } = this.props;
    if (!show && showId) {
      getShow(showId);
    }

    if (!episode) {
      getEpisode(showId, seasonId, episodeId);
    }
  }

  render() {
    const { episode, asyncErrorValue, show, isLoading } = this.props;
    return (
      <div>
        <div className="sticky">
          <Home />
        </div>
        <hr />
        {show && episode && (
          <EpisodeResult show={show.show.name} episode={episode} />
        )}
        {isLoading && <p>Loading Episode...</p>}
        {asyncErrorValue && <p>Episode not found</p>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const urlMatch = window.location.pathname.match(/\d+/g);

  return {
    isLoading: state.shows.isLoading,
    asyncErrorValue: state.shows.asyncErrorValue,
    showId: urlMatch === null ? null : urlMatch[0],
    seasonId: urlMatch === null ? null : urlMatch[1],
    episodeId: urlMatch === null ? null : urlMatch[2],
    episode: state.episodes.episode,
    show: state.shows.show
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getShow,
      getEpisode
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Episode);
