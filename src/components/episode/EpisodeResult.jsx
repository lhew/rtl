import React from "react";
import { markup } from "../../utils/";
export default ({ show, episode }) => {

  const { name, image, summary, season, number } = episode;
  return (
    <div className="result-block">
      <h3>
        {show && episode && (
          <span>
            {show} - {episode.name}
          </span>
        )}
      </h3>
      {season && number && (
        <small>
          Season {season}, episode {number}
        </small>
      )}
      <div>
        {image && image.medium && <img src={image.medium} alt={name} />}
        <div dangerouslySetInnerHTML={markup(summary)} />
      </div>
    </div>
  );
};
