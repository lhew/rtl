import React from 'react';
import ReactTestRenderer from "react-test-renderer";
import EpisodeResult from "./EpisodeResult";

describe("EpisodeResult", () => {
  it("should render properly", () => {
    const props = {
      show: "Altered Carbon", 
      episode: {
        id: 1389598,
        url:
          "http://www.tvmaze.com/episodes/1389598/altered-carbon-1x05-the-wrong-man",
        name: "The Wrong Man",
        season: 1,
        number: 5,
        airdate: "2018-02-02",
        airstamp: "2018-02-02T12:00:00+00:00",
        airtime: "",
        image: {
          medium:
            "http://static.tvmaze.com/uploads/images/medium_landscape/145/362826.jpg",
          original:
            "http://static.tvmaze.com/uploads/images/original_untouched/145/362826.jpg"
        },
        runtime: 53,
        summary:
          "<p>After learning his sleeve's identity, Kovacs demands the full story from Ortega. A tip from Poe leads to a major breakthrough in the Bancroft case.</p>",
      }
    };
    const renderer = ReactTestRenderer.create(
      <EpisodeResult {...props} />
    ).toJSON();

    expect(renderer).toMatchSnapshot();
  });
});
