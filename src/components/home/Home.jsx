import React from "react";

export default ({showName}) => (
  <div>
    <h3>Search for a show</h3>
    <form method="get" action="/results">
      <input type="search" name="term" defaultValue={showName} /> <button type="submit">Search</button>
    </form>
  </div>
);
