import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getShows } from "../../actions";
import ShowResult from "./ShowResult";
import Home from "../home/Home";
import extractQueryString from '../../utils/';


const Results = ({ isLoading, shows, getShows, searchTerm, asyncErrorValue }) => {
  if (searchTerm && shows.length === 0 && !asyncErrorValue) {
    getShows(searchTerm);
  }

  return (
    <div>
      <div className="sticky">
        <Home showName={searchTerm} />
      </div>
      <hr />
      {searchTerm && <p>Results for {searchTerm}</p>}
      {isLoading && <p>Loading results...</p>}
      {shows && shows.map((show, i) => <ShowResult key={i} {...show} />)}
      {asyncErrorValue && <p>No results found</p>}
    </div>
  );
};

const mapStateToProps = state => ({
  isLoading: state.shows.isLoading,
  asyncErrorValue: state.shows.asyncErrorValue,
  searchTerm: extractQueryString(window.location.search).term || "",
  shows: state.shows.all
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getShows
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Results);
