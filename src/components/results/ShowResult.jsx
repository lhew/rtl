import React from "react";
import { Link } from "react-router-dom";
import { markup } from "../../utils/";
export default props => {
  const {
    show: { id, name, image, summary: description }
  } = props;

  return (
    <div className="result-block">
      <Link to={"/show/" + id}>
        <img src={image && image.medium ? image.medium : ""} alt={name} />
        <div>
          <h5>{name}</h5>
          <div dangerouslySetInnerHTML={markup(description)} />
        </div>
      </Link>
    </div>
  );
};
