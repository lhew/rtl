import React from "react";
import { Link } from "react-router-dom";

export default props => {
  const { episodes, showID } = props;
  return (
    <div className="result-episodes-block">
      {episodes && episodes.length > 0 && (
        <div>
          <h4>Episodes ({episodes.length})</h4>
          <ol>
            {episodes.map(({ name, id, season, number }) => (
              <li key={id}>
                <Link
                  to={"/show/" + showID + "/episode/" + season + "/" + number}
                >
                  {name}
                </Link>
              </li>
            ))}
          </ol>
        </div>
      )}
    </div>
  );
};
