import React from "react";
import ReactTestRenderer from "react-test-renderer";
import EpisodesList from "./EpisodesList";
import { StaticRouter } from "react-router";

describe("EpisodesList", () => {
  it("should render properly", () => {
    const props = {
      showId: "300",
      episodes: [
        {
          id: 735604,
          url:
            "http://www.tvmaze.com/episodes/735604/gugu-datte-neko-de-aru-1x01-episode-1",
          name: "Episode 1",
          season: 1,
          number: 1,
          airdate: "2014-10-18",
          airtime: "22:00",
          airstamp: "2014-10-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: {
            self: {
              href: "http://api.tvmaze.com/episodes/735604"
            }
          }
        },
        {
          id: 735605,
          url:
            "http://www.tvmaze.com/episodes/735605/gugu-datte-neko-de-aru-1x02-episode-2",
          name: "Episode 2",
          season: 1,
          number: 2,
          airdate: "2014-10-25",
          airtime: "22:00",
          airstamp: "2014-10-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: {
            self: {
              href: "http://api.tvmaze.com/episodes/735605"
            }
          }
        }
      ]
    };

    const renderer = ReactTestRenderer.create(
      <StaticRouter location="some" context={{}}>
        <EpisodesList {...props} />
      </StaticRouter>
    ).toJSON();

    expect(renderer).toMatchSnapshot();
  });
});
