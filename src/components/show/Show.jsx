import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getShow, getEpisodes } from "../../actions";
import ShowSingleResult from "./ShowSingleResult";
import EpisodesList from "./EpisodesList";
import Home from "../home/Home";

class Results extends React.Component {
  componentDidMount() {
    const { show, showId, getShow, getEpisodes, episodes } = this.props;
    if (!show && !episodes && showId) {
      getShow(showId[0]);
      getEpisodes(showId[0]);
    }
  }

  render() {
    const { showId, episodes, asyncErrorValue, show, isLoading } = this.props;
    return (
      <div>
        <div className="sticky">
          <Home />
        </div>
        <hr />
        {isLoading && <p>Loading results...</p>}
        {asyncErrorValue && <p>Show not found</p>}
        {show && <ShowSingleResult {...show} />}
        {episodes && <EpisodesList episodes={episodes} showID={showId[0]} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.shows.isLoading,
  asyncErrorValue: state.shows.asyncErrorValue,
  showId: window.location.pathname.match(/\d+/),
  show: state.shows.show,
  episodes: state.episodes.all
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getShow,
      getEpisodes
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Results);
