import React from "react";


export default props => {
  const {show: { name, image, summary: description }} = props;

  const markup = function(data) {
    return {__html: data};
  }

  return (
    <div className="result-block">
      <h2>{name}</h2>
      <img src={image && image.medium ? image.medium : ''} alt={name} />
      <div>
        <div dangerouslySetInnerHTML={markup(description)} />
      </div>
      <hr/>
    </div>
  );
};
