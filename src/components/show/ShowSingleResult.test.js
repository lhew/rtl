import React from "react";
import ReactTestRenderer from "react-test-renderer";
import ShowSingleResult from "./ShowSingleResult";

import { StaticRouter } from "react-router";

describe("ShowSingleResult", () => {
  it("should render properly", () => {
    const props = {
      show: {
        score: 2.2624042,
        show: {
          id: 36771,
          url: "http://www.tvmaze.com/shows/36771/the-wedding-guru",
          name: "The Wedding Guru",
          type: "Reality",
          language: "English",
          genres: [],
          status: "Ended",
          runtime: 30,
          premiered: "2018-05-01",
          officialSite: "https://www.bbc.co.uk/programmes/b0b16ds3",
          schedule: {
            time: "22:40",
            days: ["Tuesday"]
          },
          rating: {
            average: null
          },
          weight: 62,
          network: {
            id: 352,
            name: "BBC One Wales",
            country: {
              name: "United Kingdom",
              code: "GB",
              timezone: "Europe/London"
            }
          },
          webChannel: null,
          externals: {
            tvrage: null,
            thetvdb: 347352,
            imdb: null
          },
          image: null,
          summary:
            "<p>Documentary series following Onkar Singh Purewal, the self-styled 'greatest weddings planner the world has ever seen', as he battles to create the perfect big day for his brides.</p>",
          updated: 1533371506,
          _links: {
            self: {
              href: "http://api.tvmaze.com/shows/36771"
            },
            previousepisode: {
              href: "http://api.tvmaze.com/episodes/1468143"
            }
          }
        }
      }
    };
    const renderer = ReactTestRenderer.create(
      <ShowSingleResult {...props} />
    ).toJSON();

    expect(renderer).toMatchSnapshot();
  });
});
