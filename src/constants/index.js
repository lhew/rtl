//base path constants
export const BASE_URL              = process.env.REACT_APP_SHOWS_URL;

export const SHOWS_PATH            = BASE_URL + 'search/shows/';
export const SINGLE_SHOW_PATH      = BASE_URL + 'shows/';
//reducers constants
export const GET_SHOWS_ASYNC_ERROR = 'GET_SHOWS_ASYNC_ERROR';
export const GET_SHOWS_ASYNC       = 'GET_SHOWS_ASYNC';
export const GET_SHOWS             = 'GET_SHOWS';
export const GET_SHOW_SEARCHNAME   = 'GET_SHOW_SEARCHNAME';

export const GET_SHOW              = 'GET_SHOW';
export const GET_SHOW_ASYNC        = 'GET_SHOW_ASYNC';
export const GET_SHOW_ASYNC_ERROR  = 'GET_SHOW_ASYNC_ERROR';
export const GET_SHOW_LOADING      = 'GET_SHOW_LOADING';

export const GET_EPISODES              = 'GET_EPISODES';
export const GET_EPISODES_ASYNC        = 'GET_EPISODES_ASYNC';
export const GET_EPISODES_ASYNC_ERROR  = 'GET_EPISODES_ASYNC_ERROR';
export const GET_EPISODES_LOADING      = 'GET_EPISODES_LOADING';

export const GET_EPISODE              = 'GET_EPISODE';
export const GET_EPISODE_ASYNC        = 'GET_EPISODE_ASYNC';
export const GET_EPISODE_ASYNC_ERROR  = 'GET_EPISODE_ASYNC_ERROR';
export const GET_EPISODE_LOADING      = 'GET_EPISODE_LOADING';

