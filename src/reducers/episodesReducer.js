import {
  GET_EPISODES_ASYNC,
  GET_EPISODES_ASYNC_ERROR,
  GET_EPISODE_ASYNC,
  GET_EPISODE_LOADING,
  GET_EPISODE_ASYNC_ERROR
} from "../constants";

const initialState = {
  all: null,
  episode: null,
  isLoading: false,
  asyncError: false,
  asyncErrorValue: null
};
const episodesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_EPISODES_ASYNC:
      return { ...initialState, all: action.value };

    case GET_EPISODES_ASYNC_ERROR:
      return {
        ...state,
        all: [],
        isLoading: false,
        asyncError: true,
        asyncErrorValue: action.value
      };

    case GET_EPISODE_ASYNC:
      return { ...initialState, episode: action.value };

    case GET_EPISODE_ASYNC_ERROR:
      return {
        ...state,
        isLoading: false,
        asyncError: true,
        asyncErrorValue: action.value
      };

    case GET_EPISODE_LOADING:
      return { ...state, isLoading: true, asyncErrorValue: null };

    default:
      return state;
  }
};

export default episodesReducer;
