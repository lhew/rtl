import { combineReducers} from 'redux';
import showsReducer from './showsReducer'
import episodesReducer from './episodesReducer'

export default combineReducers({
    shows: showsReducer,
    episodes: episodesReducer
});