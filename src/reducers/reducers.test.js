import reducers from "./index";

test("reducers", () => {
  let state;

  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    { type: "GET_SHOW", showID: "16134" }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: null,
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: null,
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    { type: "GET_SHOW_LOADING" }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: null,
      episode: null,
      isLoading: true,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: null,
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: true,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    { type: "GET_EPISODES", showID: "16134" }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: null,
      episode: null,
      isLoading: true,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: null,
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: true,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    { type: "GET_EPISODES_LOADING" }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: null,
      episode: null,
      isLoading: true,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: null,
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: true,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: null,
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    {
      type: "GET_EPISODES_ASYNC",
      value: [
        {
          id: 735604,
          url:
            "http://www.tvmaze.com/episodes/735604/gugu-datte-neko-de-aru-1x01-episode-1",
          name: "Episode 1",
          season: 1,
          number: 1,
          airdate: "2014-10-18",
          airtime: "22:00",
          airstamp: "2014-10-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735604" } }
        },
        {
          id: 735605,
          url:
            "http://www.tvmaze.com/episodes/735605/gugu-datte-neko-de-aru-1x02-episode-2",
          name: "Episode 2",
          season: 1,
          number: 2,
          airdate: "2014-10-25",
          airtime: "22:00",
          airstamp: "2014-10-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735605" } }
        },
        {
          id: 735607,
          url:
            "http://www.tvmaze.com/episodes/735607/gugu-datte-neko-de-aru-1x03-episode-3",
          name: "Episode 3",
          season: 1,
          number: 3,
          airdate: "2014-11-01",
          airtime: "22:00",
          airstamp: "2014-11-01T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735607" } }
        },
        {
          id: 735610,
          url:
            "http://www.tvmaze.com/episodes/735610/gugu-datte-neko-de-aru-1x04-episode-4",
          name: "Episode 4",
          season: 1,
          number: 4,
          airdate: "2014-11-08",
          airtime: "22:00",
          airstamp: "2014-11-08T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735610" } }
        },
        {
          id: 735612,
          url:
            "http://www.tvmaze.com/episodes/735612/gugu-datte-neko-de-aru-2x01-episode-1",
          name: "Episode 1",
          season: 2,
          number: 1,
          airdate: "2016-06-11",
          airtime: "22:00",
          airstamp: "2016-06-11T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735612" } }
        },
        {
          id: 735613,
          url:
            "http://www.tvmaze.com/episodes/735613/gugu-datte-neko-de-aru-2x02-episode-2",
          name: "Episode 2",
          season: 2,
          number: 2,
          airdate: "2016-06-18",
          airtime: "22:00",
          airstamp: "2016-06-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735613" } }
        },
        {
          id: 735614,
          url:
            "http://www.tvmaze.com/episodes/735614/gugu-datte-neko-de-aru-2x03-episode-3",
          name: "Episode 3",
          season: 2,
          number: 3,
          airdate: "2016-06-25",
          airtime: "22:00",
          airstamp: "2016-06-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735614" } }
        },
        {
          id: 735615,
          url:
            "http://www.tvmaze.com/episodes/735615/gugu-datte-neko-de-aru-2x04-episode-4",
          name: "Episode 4",
          season: 2,
          number: 4,
          airdate: "2016-07-02",
          airtime: "22:00",
          airstamp: "2016-07-02T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735615" } }
        },
        {
          id: 735616,
          url:
            "http://www.tvmaze.com/episodes/735616/gugu-datte-neko-de-aru-2x05-episode-5",
          name: "Episode 5",
          season: 2,
          number: 5,
          airdate: "2016-07-09",
          airtime: "22:00",
          airstamp: "2016-07-09T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735616" } }
        }
      ]
    }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: null,
      episode: null,
      isLoading: true,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: [
        {
          id: 735604,
          url:
            "http://www.tvmaze.com/episodes/735604/gugu-datte-neko-de-aru-1x01-episode-1",
          name: "Episode 1",
          season: 1,
          number: 1,
          airdate: "2014-10-18",
          airtime: "22:00",
          airstamp: "2014-10-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735604" } }
        },
        {
          id: 735605,
          url:
            "http://www.tvmaze.com/episodes/735605/gugu-datte-neko-de-aru-1x02-episode-2",
          name: "Episode 2",
          season: 1,
          number: 2,
          airdate: "2014-10-25",
          airtime: "22:00",
          airstamp: "2014-10-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735605" } }
        },
        {
          id: 735607,
          url:
            "http://www.tvmaze.com/episodes/735607/gugu-datte-neko-de-aru-1x03-episode-3",
          name: "Episode 3",
          season: 1,
          number: 3,
          airdate: "2014-11-01",
          airtime: "22:00",
          airstamp: "2014-11-01T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735607" } }
        },
        {
          id: 735610,
          url:
            "http://www.tvmaze.com/episodes/735610/gugu-datte-neko-de-aru-1x04-episode-4",
          name: "Episode 4",
          season: 1,
          number: 4,
          airdate: "2014-11-08",
          airtime: "22:00",
          airstamp: "2014-11-08T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735610" } }
        },
        {
          id: 735612,
          url:
            "http://www.tvmaze.com/episodes/735612/gugu-datte-neko-de-aru-2x01-episode-1",
          name: "Episode 1",
          season: 2,
          number: 1,
          airdate: "2016-06-11",
          airtime: "22:00",
          airstamp: "2016-06-11T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735612" } }
        },
        {
          id: 735613,
          url:
            "http://www.tvmaze.com/episodes/735613/gugu-datte-neko-de-aru-2x02-episode-2",
          name: "Episode 2",
          season: 2,
          number: 2,
          airdate: "2016-06-18",
          airtime: "22:00",
          airstamp: "2016-06-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735613" } }
        },
        {
          id: 735614,
          url:
            "http://www.tvmaze.com/episodes/735614/gugu-datte-neko-de-aru-2x03-episode-3",
          name: "Episode 3",
          season: 2,
          number: 3,
          airdate: "2016-06-25",
          airtime: "22:00",
          airstamp: "2016-06-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735614" } }
        },
        {
          id: 735615,
          url:
            "http://www.tvmaze.com/episodes/735615/gugu-datte-neko-de-aru-2x04-episode-4",
          name: "Episode 4",
          season: 2,
          number: 4,
          airdate: "2016-07-02",
          airtime: "22:00",
          airstamp: "2016-07-02T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735615" } }
        },
        {
          id: 735616,
          url:
            "http://www.tvmaze.com/episodes/735616/gugu-datte-neko-de-aru-2x05-episode-5",
          name: "Episode 5",
          season: 2,
          number: 5,
          airdate: "2016-07-09",
          airtime: "22:00",
          airstamp: "2016-07-09T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735616" } }
        }
      ],
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
  state = reducers(
    {
      shows: {
        all: [],
        show: null,
        episode: null,
        isLoading: true,
        asyncError: false,
        asyncErrorValue: null
      },
      episodes: {
        all: [
          {
            id: 735604,
            url:
              "http://www.tvmaze.com/episodes/735604/gugu-datte-neko-de-aru-1x01-episode-1",
            name: "Episode 1",
            season: 1,
            number: 1,
            airdate: "2014-10-18",
            airtime: "22:00",
            airstamp: "2014-10-18T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735604" } }
          },
          {
            id: 735605,
            url:
              "http://www.tvmaze.com/episodes/735605/gugu-datte-neko-de-aru-1x02-episode-2",
            name: "Episode 2",
            season: 1,
            number: 2,
            airdate: "2014-10-25",
            airtime: "22:00",
            airstamp: "2014-10-25T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735605" } }
          },
          {
            id: 735607,
            url:
              "http://www.tvmaze.com/episodes/735607/gugu-datte-neko-de-aru-1x03-episode-3",
            name: "Episode 3",
            season: 1,
            number: 3,
            airdate: "2014-11-01",
            airtime: "22:00",
            airstamp: "2014-11-01T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735607" } }
          },
          {
            id: 735610,
            url:
              "http://www.tvmaze.com/episodes/735610/gugu-datte-neko-de-aru-1x04-episode-4",
            name: "Episode 4",
            season: 1,
            number: 4,
            airdate: "2014-11-08",
            airtime: "22:00",
            airstamp: "2014-11-08T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735610" } }
          },
          {
            id: 735612,
            url:
              "http://www.tvmaze.com/episodes/735612/gugu-datte-neko-de-aru-2x01-episode-1",
            name: "Episode 1",
            season: 2,
            number: 1,
            airdate: "2016-06-11",
            airtime: "22:00",
            airstamp: "2016-06-11T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735612" } }
          },
          {
            id: 735613,
            url:
              "http://www.tvmaze.com/episodes/735613/gugu-datte-neko-de-aru-2x02-episode-2",
            name: "Episode 2",
            season: 2,
            number: 2,
            airdate: "2016-06-18",
            airtime: "22:00",
            airstamp: "2016-06-18T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735613" } }
          },
          {
            id: 735614,
            url:
              "http://www.tvmaze.com/episodes/735614/gugu-datte-neko-de-aru-2x03-episode-3",
            name: "Episode 3",
            season: 2,
            number: 3,
            airdate: "2016-06-25",
            airtime: "22:00",
            airstamp: "2016-06-25T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735614" } }
          },
          {
            id: 735615,
            url:
              "http://www.tvmaze.com/episodes/735615/gugu-datte-neko-de-aru-2x04-episode-4",
            name: "Episode 4",
            season: 2,
            number: 4,
            airdate: "2016-07-02",
            airtime: "22:00",
            airstamp: "2016-07-02T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735615" } }
          },
          {
            id: 735616,
            url:
              "http://www.tvmaze.com/episodes/735616/gugu-datte-neko-de-aru-2x05-episode-5",
            name: "Episode 5",
            season: 2,
            number: 5,
            airdate: "2016-07-09",
            airtime: "22:00",
            airstamp: "2016-07-09T13:00:00+00:00",
            runtime: 60,
            image: null,
            summary: "",
            _links: { self: { href: "http://api.tvmaze.com/episodes/735616" } }
          }
        ],
        episode: null,
        isLoading: false,
        asyncError: false,
        asyncErrorValue: null
      }
    },
    {
      type: "GET_SHOW_ASYNC",
      value: {
        id: 16134,
        url: "http://www.tvmaze.com/shows/16134/gugu-datte-neko-de-aru",
        name: "Gugu datte Neko de Aru",
        type: "Scripted",
        language: "Japanese",
        genres: ["Drama"],
        status: "Ended",
        runtime: 60,
        premiered: "2014-10-18",
        officialSite: null,
        schedule: { time: "22:00", days: ["Saturday"] },
        rating: { average: null },
        weight: 0,
        network: {
          id: 158,
          name: "WOWOW",
          country: { name: "Japan", code: "JP", timezone: "Asia/Tokyo" }
        },
        webChannel: null,
        externals: { tvrage: null, thetvdb: 286480, imdb: null },
        image: {
          medium:
            "http://static.tvmaze.com/uploads/images/medium_portrait/53/134950.jpg",
          original:
            "http://static.tvmaze.com/uploads/images/original_untouched/53/134950.jpg"
        },
        summary:
          "<p>Manga writer Kojima Asako's lovely days with her cats, her editor Omori's marriage, assistant Manami's independence and the whereabouts of the missing homeless man.</p>",
        updated: 1468065525,
        _links: {
          self: { href: "http://api.tvmaze.com/shows/16134" },
          previousepisode: { href: "http://api.tvmaze.com/episodes/735616" }
        }
      }
    }
  );
  expect(state).toEqual({
    shows: {
      all: [],
      show: {
        show: {
          id: 16134,
          url: "http://www.tvmaze.com/shows/16134/gugu-datte-neko-de-aru",
          name: "Gugu datte Neko de Aru",
          type: "Scripted",
          language: "Japanese",
          genres: ["Drama"],
          status: "Ended",
          runtime: 60,
          premiered: "2014-10-18",
          officialSite: null,
          schedule: { time: "22:00", days: ["Saturday"] },
          rating: { average: null },
          weight: 0,
          network: {
            id: 158,
            name: "WOWOW",
            country: { name: "Japan", code: "JP", timezone: "Asia/Tokyo" }
          },
          webChannel: null,
          externals: { tvrage: null, thetvdb: 286480, imdb: null },
          image: {
            medium:
              "http://static.tvmaze.com/uploads/images/medium_portrait/53/134950.jpg",
            original:
              "http://static.tvmaze.com/uploads/images/original_untouched/53/134950.jpg"
          },
          summary:
            "<p>Manga writer Kojima Asako's lovely days with her cats, her editor Omori's marriage, assistant Manami's independence and the whereabouts of the missing homeless man.</p>",
          updated: 1468065525,
          _links: {
            self: { href: "http://api.tvmaze.com/shows/16134" },
            previousepisode: { href: "http://api.tvmaze.com/episodes/735616" }
          }
        }
      },
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    },
    episodes: {
      all: [
        {
          id: 735604,
          url:
            "http://www.tvmaze.com/episodes/735604/gugu-datte-neko-de-aru-1x01-episode-1",
          name: "Episode 1",
          season: 1,
          number: 1,
          airdate: "2014-10-18",
          airtime: "22:00",
          airstamp: "2014-10-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735604" } }
        },
        {
          id: 735605,
          url:
            "http://www.tvmaze.com/episodes/735605/gugu-datte-neko-de-aru-1x02-episode-2",
          name: "Episode 2",
          season: 1,
          number: 2,
          airdate: "2014-10-25",
          airtime: "22:00",
          airstamp: "2014-10-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735605" } }
        },
        {
          id: 735607,
          url:
            "http://www.tvmaze.com/episodes/735607/gugu-datte-neko-de-aru-1x03-episode-3",
          name: "Episode 3",
          season: 1,
          number: 3,
          airdate: "2014-11-01",
          airtime: "22:00",
          airstamp: "2014-11-01T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735607" } }
        },
        {
          id: 735610,
          url:
            "http://www.tvmaze.com/episodes/735610/gugu-datte-neko-de-aru-1x04-episode-4",
          name: "Episode 4",
          season: 1,
          number: 4,
          airdate: "2014-11-08",
          airtime: "22:00",
          airstamp: "2014-11-08T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735610" } }
        },
        {
          id: 735612,
          url:
            "http://www.tvmaze.com/episodes/735612/gugu-datte-neko-de-aru-2x01-episode-1",
          name: "Episode 1",
          season: 2,
          number: 1,
          airdate: "2016-06-11",
          airtime: "22:00",
          airstamp: "2016-06-11T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735612" } }
        },
        {
          id: 735613,
          url:
            "http://www.tvmaze.com/episodes/735613/gugu-datte-neko-de-aru-2x02-episode-2",
          name: "Episode 2",
          season: 2,
          number: 2,
          airdate: "2016-06-18",
          airtime: "22:00",
          airstamp: "2016-06-18T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735613" } }
        },
        {
          id: 735614,
          url:
            "http://www.tvmaze.com/episodes/735614/gugu-datte-neko-de-aru-2x03-episode-3",
          name: "Episode 3",
          season: 2,
          number: 3,
          airdate: "2016-06-25",
          airtime: "22:00",
          airstamp: "2016-06-25T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735614" } }
        },
        {
          id: 735615,
          url:
            "http://www.tvmaze.com/episodes/735615/gugu-datte-neko-de-aru-2x04-episode-4",
          name: "Episode 4",
          season: 2,
          number: 4,
          airdate: "2016-07-02",
          airtime: "22:00",
          airstamp: "2016-07-02T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735615" } }
        },
        {
          id: 735616,
          url:
            "http://www.tvmaze.com/episodes/735616/gugu-datte-neko-de-aru-2x05-episode-5",
          name: "Episode 5",
          season: 2,
          number: 5,
          airdate: "2016-07-09",
          airtime: "22:00",
          airstamp: "2016-07-09T13:00:00+00:00",
          runtime: 60,
          image: null,
          summary: "",
          _links: { self: { href: "http://api.tvmaze.com/episodes/735616" } }
        }
      ],
      episode: null,
      isLoading: false,
      asyncError: false,
      asyncErrorValue: null
    }
  });
});
