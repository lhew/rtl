import {
  GET_SHOWS_ASYNC,
  GET_SHOWS_ASYNC_ERROR,
  GET_SHOW_ASYNC,
  GET_SHOW_LOADING,
  GET_SHOW_ASYNC_ERROR,
  GET_SHOW_SEARCHNAME
} from "../constants";

const initialState = {
  all: [],
  show: null,
  episode: null,
  isLoading: false,
  asyncError: false,
  asyncErrorValue: null
};
const showsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SHOW_SEARCHNAME:
      return { ...state, searchName: action.value, asyncErrorValue: null };

    case GET_SHOWS_ASYNC:
      return { ...initialState, all: action.value };

    case GET_SHOWS_ASYNC_ERROR:
      return {
        ...state,
        all: [],
        isLoading: false,
        asyncError: true,
        asyncErrorValue: action.value
      };

    case GET_SHOW_ASYNC:
      return { ...initialState, show: { show: action.value } };

    case GET_SHOW_ASYNC_ERROR:
      return {
        ...state,
        isLoading: false,
        asyncError: true,
        asyncErrorValue: action.value
      };

    case GET_SHOW_LOADING:
      return { ...state, isLoading: true, asyncErrorValue: null };

    default:
      return state;
  }
};

export default showsReducer;
