import { takeLatest, put, call } from "redux-saga/effects";
import {
  requestShows,
  requestShow,
  requestEpisodes,
  requestEpisode
} from "../api";
import {
  GET_SHOWS_ASYNC_ERROR,
  GET_SHOWS_ASYNC,
  GET_SHOWS,
  GET_SHOW_ASYNC,
  GET_SHOW_ASYNC_ERROR,
  GET_SHOW_SEARCHNAME,
  GET_SHOW,
  GET_SHOW_LOADING,
  GET_EPISODES,
  GET_EPISODES_ASYNC,
  GET_EPISODES_LOADING,
  GET_EPISODES_ASYNC_ERROR,
  GET_EPISODE,
  GET_EPISODE_ASYNC,
  GET_EPISODE_LOADING,
  GET_EPISODE_ASYNC_ERROR
} from "../constants";

function* getShowsAsync(payload) {
  try {
    const data = yield call(requestShows, payload.showName);
    yield put({ type: GET_SHOW_SEARCHNAME, value: payload.showName });
    if (data.length > 0) {
      yield put({ type: GET_SHOWS_ASYNC, value: data });
    } else {
      yield put({ type: GET_SHOWS_ASYNC_ERROR, value: "no-shows" });
    }
  } catch (e) {
    yield put({ type: GET_SHOWS_ASYNC_ERROR });
  }
}

function* getShowAsync(payload) {
  yield put({ type: GET_SHOW_LOADING });

  try {
    const data = yield call(requestShow, payload.showID);

    if (data || data.length > 0) {
      yield put({ type: GET_SHOW_ASYNC, value: data });
    } else {
      yield put({ type: GET_SHOW_ASYNC_ERROR, value: "no-show" });
    }
  } catch (e) {
    yield put({ type: GET_SHOW_ASYNC_ERROR });
  }
}

function* getEpisodesAsync(payload) {
  yield put({ type: GET_EPISODES_LOADING });

  try {
    const data = yield call(requestEpisodes, payload.showID);

    if (data || data.length > 0) {
      yield put({ type: GET_EPISODES_ASYNC, value: data });
    } else {
      yield put({ type: GET_EPISODES_ASYNC_ERROR, value: "no-show" });
    }
  } catch (e) {
    yield put({ type: GET_EPISODES_ASYNC_ERROR });
  }
}

function* getEpisodeAsync(payload) {
  yield put({ type: GET_EPISODE_LOADING });

  try {
    const data = yield call(
      requestEpisode,
      payload.showID,
      payload.season,
      payload.episode
    );

    if (data || data.length > 0) {
      yield put({ type: GET_EPISODE_ASYNC, value: data });
    } else {
      yield put({ type: GET_EPISODE_ASYNC_ERROR, value: "no-episode" });
    }
  } catch (e) {
    yield put({ type: GET_EPISODE_ASYNC_ERROR });
  }
}

export default function* root(data) {
  yield takeLatest(GET_SHOW, getShowAsync);
  yield takeLatest(GET_SHOWS, getShowsAsync);
  yield takeLatest(GET_EPISODES, getEpisodesAsync);
  yield takeLatest(GET_EPISODE, getEpisodeAsync);
}
