export default str => {
  let term = str
    .split("?")[1]
    .split("&")
    .map(expr => {
      var obj = {};
      obj[expr.split("=")[0]] = expr.split("=")[1];
      return obj;
    });

  return term[0];
};

export const markup = function(data) {
  return { __html: data };
};
